# Car Management API 

This repository implements car data management with authentication and authorization feature.

# Getting Started

First of all, install all the node modules with :

```sh
npm install
```

After that, create `.env` file whose contents are the same as [`.env-example`](.env-example), but fill it with your own. For instance,

```sh
DB_USERNAME = postgres
DB_PASSWORD = 12345
DB_NAME = Car-Management
DB_HOST = 127.0.0.1 
PORT = 8000 
SUPERADMIN_PASSWORD = araara123
JWT_PRIVATE_KEY = hutaocantik
```

To run the server, run the script `dev` inside `package.json` file with : 

```sh
npm run dev
```
## Database Management

- `sequelize db:create` used to create a database
- `sequelize db:drop` used to deleting database
- `sequelize db:migrate` used to run latest migration
- `sequelize db:seed:all` used to do all the seeding
- `sequelize db:migrate:undo` used to cancel latest migration

# Superadmin Data
Inside the [`./db/seeders`](./db/seeders), there is a file that contains data for superadmin while the data are email, username, and password. Superadmin's password are taken from variable `SUPERADMIN_PASSWORD` in the `.env` file. With the example above, superadmin's password is `araara123`. Thus the superadmin's data are : 
```json
  {
    "email": "iloveyou123@gmail.com",
    "username": "nino123",
    "password": "araara123"
  }
```
# Open API Documentation
Please go to endpoint `/api/v1/docs` to access open API documentation page.